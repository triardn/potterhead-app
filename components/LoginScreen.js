import React, { Component } from "react"
import { StyleSheet, View, Text, TextInput, Button, TouchableOpacity, Image } from "react-native"
import MaterialCommunityIcons from "@expo/vector-icons/MaterialCommunityIcons"
import { AppConsumer } from './UserContext'

export default class App extends Component {
    constructor(props) {
        super(props)
        this.state = {
            userName: '',
            password: '',
            isError: false,
        }
    }

    loginHandler(setUserName) {
        if (this.state.password == 'potterhead') {
            setUserName(this.state.userName)
            this.props.navigation.push('Houses', {userName: this.state.userName})
        } else {
            this.setState({ isError: true })
        }
    }

    render() {
        return (
            <AppConsumer>
                {({ setUserName }) => (
                    <View style={styles.container}>
                        <View>
                            <Image source={require('../assets/PA.png')} style={styles.logo} />
                        </View>
        
                        <View style={styles.formContainer}>
                            <View style={styles.inputContainer}>
                                <MaterialCommunityIcons
                                name="account-circle"
                                color="#9C0000"
                                size={40}
                                />
                                <View style={{ marginLeft: 10 }}>
                                    <Text style={styles.labelText}>Username</Text>
                                    <TextInput
                                        style={styles.textInput}
                                        placeholder="Masukkan Username"
                                        onChangeText={(userName) => this.setState({ userName })}
                                    />
                                </View>
                            </View>
        
                            <View style={styles.inputContainer}>
                                <MaterialCommunityIcons name="lock" color="#9C0000" size={40} />
                                <View style={{ marginLeft: 10 }}>
                                    <Text style={styles.labelText}>Password</Text>
                                    <TextInput
                                        style={styles.textInput}
                                        placeholder="Masukkan Password"
                                        onChangeText={(password) => this.setState({ password })}
                                        secureTextEntry={true}
                                    />
                                </View>
                            </View>
                            <Text
                                style={
                                    this.state.isError ? styles.errorText : styles.hiddenErrorText
                                }
                            >
                                Password Salah
                            </Text>
                            <View style={styles.loginContainer}>
                                <TouchableOpacity style={styles.loginButton} onPress={() => this.loginHandler(setUserName)}>
                                    <Text style={ styles.loginLabel }>Login</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                )}
            </AppConsumer>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 2,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#FFFFFF',
    },
    titleText: {
        fontSize: 48,
        fontWeight: 'bold',
        color: 'blue',
        textAlign: 'center',
    },
    subTitleText: {
        fontSize: 24,
        fontWeight: 'bold',
        color: 'blue',
        alignSelf: 'flex-end',
        marginBottom: 16
    },
    formContainer: {
        justifyContent: 'center',
        marginTop: 50
    },
    inputContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        alignSelf: 'center',
        marginBottom: 16,
    },
    labelText: {
        fontWeight: 'bold'
    },
    textInput: {
        width: 200,
        height: 30,
        backgroundColor: 'white',
        borderWidth: 1,
        paddingLeft: 10
    },
    errorText: {
        color: 'red',
        textAlign: 'center',
        marginBottom: 16,
        fontWeight: 'bold',
    },
    hiddenErrorText: {
        color: 'transparent',
        textAlign: 'center',
        marginBottom: 16,
    },
    logo: {
        width: 200,
        height: 200,

    },
    loginContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    loginButton: {
        height: 50,
        width: 125,
        backgroundColor: '#9C0000',
        marginTop: 35,
        borderRadius: 10,
        alignItems: 'center',
        justifyContent: 'center',
    },
    loginLabel: {
        fontSize: 16,
        textAlign: 'center',
        color: '#FFFFFF',
    },
});
