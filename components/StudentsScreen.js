import React, { Component } from 'react'
import { StyleSheet, View, Text, Dimensions, ActivityIndicator, FlatList } from 'react-native'
import Axios from 'axios'

const DEVICE = Dimensions.get('window')

export default class Students extends Component {
    constructor(props) {
        super(props)
        this.state = {
            data: [],
            isLoading: true,
            isError: false,
        }
    }

    // Mount getStudent
    componentDidMount() {
        this.getAllStudent(this.props.route.params.houseName)
    }

    getAllStudent = async (houseName) => {
        try {
            const response = await Axios.get(`https://www.potterapi.com/v1/characters?key=$2a$10$PfbrvoS/LMLg8jSCEkW6.OuzU18mJlrhKm/VBeYkNywSCNnXt0PTC&house=${houseName}&role=student`)
            this.setState({ isError: false, isLoading: false, data: response.data })
        } catch (error) {
            this.setState({ isError: true, isLoading: false })
        }
    }

    getLabel(args) {
        if (args) {
            return <Text style={{ color: 'green', fontWeight: 'bold' }}>Yes</Text>
        } else {
            return <Text style={{ color: 'red', fontWeight: 'bold' }}>No</Text>
        }
    }

    render() {
        //  If load data
        if (this.state.isLoading) {
            return (
                <View
                    style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}
                >
                    <ActivityIndicator size='large' color='red' />
                </View>
            )
        }

        // If data not fetch
        else if (this.state.isError) {
            return (
                <View
                    style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}
                >
                    <Text>Something happen when fetch the data. Please try again later.</Text>
                </View>
            )
        }

        // If data finish load
        return (
            <View style={ styles.container }>
                <View style={ styles.headerContainer }>
                    <Text style={ styles.headerTitle }>{this.props.route.params.houseName} Students</Text>
                </View>
                <FlatList
                    data={this.state.data}
                    renderItem={({ item }) =>
                        <View style={styles.viewList}>
                            <View>
                                <Text style={styles.textItemLogin}> { item.name } - { item.bloodStatus }</Text>
                                <Text style={styles.textItemUrl}>Is Member of Order of Phoenix: { this.getLabel(item.orderOfThePhoenix) }</Text>
                                <Text style={styles.textItemUrl}>Is Member of Dumbledore's Army: { this.getLabel(item.dumbledoresArmy) }</Text>
                            </View>
                        </View>
                    }
                    keyExtractor={({ id }, index) => `list-item${index}`}
                    style={{ marginBottom: 30 }}
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    itemContainer: {
        width: DEVICE.width * 0.50,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#FFFFFF',
        margin: 5,
        height: DEVICE.height * 0.30,
    },
    headerContainer: {
        marginTop: 50,
        marginBottom: 30,
    },
    headerTitle: {
        fontSize: 28,
        fontWeight: 'bold'
    },
    viewList: {
        height: 120,
        width: DEVICE.width * 0.80,
        flexDirection: 'row',
        borderWidth: 1,
        borderColor: '#DDD',
        alignItems: 'center',
    },
    Image: {
        width: 88,
        height: 80,
        borderRadius: 40
    },
    textItemLogin: {
        fontWeight: 'bold',
        textTransform: 'capitalize',
        marginLeft: 20,
        fontSize: 16
    },
    textItemUrl: {
        fontWeight: 'bold',
        marginLeft: 20,
        fontSize: 12,
        marginTop: 10,
        color: 'black'
    },
})