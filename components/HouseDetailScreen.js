import React, { Component } from 'react'
import { StyleSheet, View, Text, Image, TouchableOpacity, ActivityIndicator } from 'react-native'
import Axios from 'axios'

// import images
import img from './images'

export default class HouseDetail extends Component {
    constructor(props) {
        super(props)
        this.state = {
            data: [],
            isLoading: true,
            isError: false,
        }
    }

    // Mount getHouseDetail
    componentDidMount() {
        this.getHouseDetail(this.props.route.params.houseID)
    }

    getHouseDetail = async (houseID) => {
        try {
            const response = await Axios.get(`https://www.potterapi.com/v1/houses/${houseID}?key=$2a$10$PfbrvoS/LMLg8jSCEkW6.OuzU18mJlrhKm/VBeYkNywSCNnXt0PTC`)
            this.setState({ isError: false, isLoading: false, data: response.data })
        } catch (error) {
            this.setState({ isError: true, isLoading: false })
        }
    }

    moveToStudentLists(houseName) {
        this.props.navigation.navigate('Students', { houseName: houseName })
    }

    render() {
        //  If load data
        if (this.state.isLoading) {
            return (
                <View
                    style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}
                >
                    <ActivityIndicator size='large' color='red' />
                </View>
            )
        }
        // If data not fetch
        else if (this.state.isError) {
            return (
                <View
                    style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}
                >
                    <Text>Something happen when fetch the data. Please try again later.</Text>
                </View>
            )
        }

        const house = this.state.data[0]
        const houseValues = house.values.join(", ")
        let houseImagePath = ''

        switch (house.name) {
            case 'Gryffindor':
                houseImagePath = img[0]
                break
            case 'Ravenclaw':
                houseImagePath = img[1]
                break
            case 'Hufflepuff':
                houseImagePath = img[2]
                break
            case 'Slytherin':
                houseImagePath = img[3]
                break
            default:
                houseImagePath = img[0]
        }

        return(
            <View styles={ styles.container }>
                <View style={ styles.titleContainer }>
                    <Image source={houseImagePath} style={styles.logo} />
                    <Text style={ styles.houseTitle }>{ house.name }</Text>
                    <Text>Founded By</Text>
                    <Text style={ styles.bold }>{ house.founder }</Text>
                </View>

                <View style={ styles.detailContainer }>
                    <View style={ styles.detailItem }>
                        <Text>Head of House: </Text>
                        <Text style={ styles.bold }>{ house.headOfHouse}</Text>
                    </View>

                    <View style={ styles.detailItem }>
                        <Text>House Ghost: </Text>
                        <Text style={ styles.bold }>{ house.houseGhost }</Text>
                    </View>

                    <View style={ styles.detailItem }>
                        <Text>Mascot: </Text>
                        <Text style={ styles.bold }>{ house.mascot }</Text>
                    </View>

                    <View style={ styles.detailItem }>
                        <Text>Value: </Text>
                        <Text style={ styles.bold }>{ houseValues }</Text>
                    </View>
                </View>

                <View style={ styles.buttonContainer }>
                    <TouchableOpacity style={ styles.buttonViewStudent } onPress={() => this.moveToStudentLists(house.name)}>
                        <Text style={{ color: '#FFFFFF' }}>View All Students</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    titleContainer: {
        marginTop: 50,
        marginBottom: 30,
        alignItems: 'center'
    },
    logo: {
        width: 250,
        height: 250,
        marginBottom: 30,
    },
    houseTitle: {
        fontSize: 22
    },
    detailContainer: {
        marginLeft: 0,
    },
    detailItem: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    buttonContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    buttonViewStudent: {
        height: 50,
        width: 200,
        backgroundColor: '#9C0000',
        marginTop: 35,
        borderRadius: 10,
        alignItems: 'center',
        justifyContent: 'center',
    },
    bold: {
        fontWeight: 'bold'
    },
})