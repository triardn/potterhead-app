import React from 'react';

export const AppContext = React.createContext({
  userName: 'dummy',
  setUserName: () => null,
});

export const AppConsumer = AppContext.Consumer;

export class AppProvider extends React.Component {
  state = {
    userName: 'dummy',
  }

  setUserName = (userName) => {
    this.setState({ userName })
  }

  render() {
    return(
      <AppContext.Provider
        value={{
          userName: this.state.userName,
          setUserName: this.setUserName,
        }}
      >
        { this.props.children }
      </AppContext.Provider>
    )
  }
}