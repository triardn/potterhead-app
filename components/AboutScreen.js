import React, { Component } from 'react'
import { StyleSheet, View, Text, Image } from 'react-native'
import MaterialCommunityIcons from "@expo/vector-icons/MaterialCommunityIcons";

export default class About extends Component {
    render() {
        return(
            <View style={ styles.container }>
                <Text style={{ fontSize: 30, fontWeight: 'bold'}}>Potterhead App</Text>
                <Text style={{ fontSize: 20, fontWeight: 'bold'}}>Developed by TechnoCraft</Text>
                
                <View style={ styles.logoContainer }>
                    <Image source={require('../assets/SortingHat.png')} style={styles.logo} />
                </View>
                
                <View style={ styles.contactMe }>
                    <Text style={{ fontSize: 20, marginBottom: 10}}>Connect with me</Text>
                    <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center'}}>
                        <View style={{ marginHorizontal: 5}}>
                            <MaterialCommunityIcons.Button name="instagram" size={24} color="white" backgroundColor="#9C0000">
                                Instagram
                            </MaterialCommunityIcons.Button>
                        </View>
                        <View style={{ marginHorizontal: 5}}>
                            <MaterialCommunityIcons.Button name="twitter-box" size={24} color="white" backgroundColor="#9C0000">
                                Twitter
                            </MaterialCommunityIcons.Button>
                        </View>
                    </View>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    contactMe: {
        margin: 50,
        alignItems: 'center',
        justifyContent: 'center',
    },
    logoContainer: { 
        marginTop: 40,
    },
    logo: {
        width: 200,
        height: 200,
    },
})