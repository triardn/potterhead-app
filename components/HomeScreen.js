import React, { Component } from 'react'
import { StyleSheet, View, Text, TouchableOpacity } from 'react-native'
import { AppConsumer } from './UserContext';

export default class Home extends Component {
    constructor(props) {
        super(props)
    }

    goToDetail(houseID) {
        this.props.navigation.navigate('HouseDetail', { houseID: houseID })
    }

    render() {
        return(
            <AppConsumer>
                {({ userName }) => (
                    <View style={ styles.container }>
                        <View style={ styles.headerContainer }>
                        <Text style={ styles.headerTitle }>Hi, { userName }!</Text>
                            <Text style={ styles.headerTitle }>Choose your Houses!!</Text>
                        </View>

                        <View style={ styles.cardContainer }>
                            <TouchableOpacity onPress={() => this.goToDetail('5a05e2b252f721a3cf2ea33f')}>
                                <View style={styles.GryffindorContainer}>
                                    <Text style={ styles.houseName }>Gryffindor</Text>
                                </View>
                            </TouchableOpacity>

                            <TouchableOpacity onPress={() => this.goToDetail('5a05da69d45bd0a11bd5e06f')}>
                                <View style={styles.RavenclawContainer}>
                                    <Text style={ styles.houseName }>Ravenclaw</Text>
                                </View>
                            </TouchableOpacity>

                            <TouchableOpacity onPress={() => this.goToDetail('5a05dc58d45bd0a11bd5e070')}>
                                <View style={styles.HufflepuffContainer}>
                                    <Text style={ styles.houseName }>Hufflepuff</Text>
                                </View>
                            </TouchableOpacity>

                            <TouchableOpacity onPress={() => this.goToDetail('5a05dc8cd45bd0a11bd5e071')}>
                                <View style={styles.SlytherinContainer}>
                                    <Text style={ styles.houseName }>Slytherin</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>
                )}
            </AppConsumer>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    headerContainer: {
        marginBottom: 30,
        alignItems: 'center'
    },
    headerTitle: {
        fontSize: 24
    },
    GryffindorContainer: {
        marginTop: 20,
        width: 120 * 3,
        height: 120,
        borderRadius: 15,
        backgroundColor: '#ae0001',
        alignItems: 'center',
        justifyContent: 'center',
    },
    RavenclawContainer: {
        marginTop: 20,
        width: 120 * 3,
        height: 120,
        borderRadius: 15,
        backgroundColor: '#222f5b',
        alignItems: 'center',
        justifyContent: 'center',
    },
    HufflepuffContainer: {
        marginTop: 20,
        width: 120 * 3,
        height: 120,
        borderRadius: 15,
        backgroundColor: '#f0c75e',
        alignItems: 'center',
        justifyContent: 'center',
    },
    SlytherinContainer: {
        marginTop: 20,
        width: 120 * 3,
        height: 120,
        borderRadius: 15,
        backgroundColor: '#2a623d',
        alignItems: 'center',
        justifyContent: 'center',
    },
    houseName: {
        fontSize: 30,
        color: '#FFFFFF',
    },
})