import React from 'react'

import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import { createDrawerNavigator } from '@react-navigation/drawer'

import Login from './LoginScreen'
import Houses from './HomeScreen'
import HouseDetail from './HouseDetailScreen'
import Students from './StudentsScreen'
import About from './AboutScreen'

const MainStack = createStackNavigator()
const LoginStack = createStackNavigator()
const HomeStack = createStackNavigator()
const HouseDetailStack = createStackNavigator()
const StudentStack = createStackNavigator()
const AboutStack = createStackNavigator()
const Drawer = createDrawerNavigator()

const AboutStackScreen = () => (
    <AboutStack.Navigator headerMode="none">
        <AboutStack.Screen name="About" component={About} options={{title: "About Us" }} />
    </AboutStack.Navigator>
)

const HomeStackScreen = () => (
    <HomeStack.Navigator headerMode="none">
        <HomeStack.Screen name="Houses" component={Houses} />
        <HomeStack.Screen name="HouseDetail" component={HouseDetailStackScreen} />
    </HomeStack.Navigator>
)

const HouseDetailStackScreen = () => (
    <HouseDetailStack.Navigator>
        <HouseDetailStack.Screen name="HouseDetail" component={HouseDetail} />
        <HouseDetailStack.Screen name="Students" component={StudentStackScreen} />
    </HouseDetailStack.Navigator>
)

const StudentStackScreen = () => (
    <StudentStack.Navigator>
        <StudentStack.Screen name="Students" component={Students} />
    </StudentStack.Navigator>
)

const HomeDrawerScreen = () => (
    <Drawer.Navigator headerMode="none" headerShown="false">
        <Drawer.Screen name="Houses" component={LoginStackScreen} options={{title: "Houses"}} screenOptions={{ headerShown: false }} />
        <Drawer.Screen name="About" component={AboutStackScreen} options={{title: "About Us"}} screenOptions={{ headerShown: false }} />
    </Drawer.Navigator>
)

const LoginStackScreen = () => (
    <LoginStack.Navigator headerMode="none" headerShown="false">
        <LoginStack.Screen name="Login" component={Login} options={{title: "Houses"}} screenOptions={{ headerShown: false }} />
        <LoginStack.Screen name="Houses" component={Houses} options={{title: "Houses"}} screenOptions={{ headerShown: false }} />
    </LoginStack.Navigator>
)

export default () => (
    <NavigationContainer>
        <MainStack.Navigator>
            <MainStack.Screen name="Login" component={HomeDrawerScreen} headerMode="none" />
            <MainStack.Screen name="HouseDetail" component={HouseDetail} options={{ title: 'House Detail' }} />
            <MainStack.Screen name="Students" component={Students} />
            <MainStack.Screen name="About" component={About} options={{ title: 'About Us' }} />
        </MainStack.Navigator>
    </NavigationContainer>
)