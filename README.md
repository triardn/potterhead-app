# Potterhead App

Using Expo

Consume API from `https://www.potterapi.com/`
- Get House detail --> (https://www.potterapi.com/v1/houses/{houseID})
- Get House students --> (https://www.potterapi.com/v1/characters?key={api-key}C&house={house-name}&role=student)

Auth password to enter the app `potterhead`

[mockup link](https://www.figma.com/file/HnOU9Mqh9ciuV4bsigTDBp/CovidMonitoring?node-id=0%3A1)
notes: (mockup akan diperbaharui karena awalnya saya ingin menggunakan API dari https://kawalcorona.com/, hanya saja sejak jumat API terserbut tidak bisa diakses, jadi saya mengubah semua desain App yg sudah dibuat menyesuaikan dengan API yg baru)