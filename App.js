import React from 'react';
import AppComponent from './components/index'
import { AppProvider } from './components/UserContext';

export default function App() {
    return (
        <AppProvider>
            <AppComponent />
        </AppProvider>
    );
}